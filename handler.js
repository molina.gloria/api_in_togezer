import Git from './src/toogo/git.js';
import Fit from './src/toogo/fit.js';

console.log("entra por handle");

export const run_data_migration = async (event, context, callback) => {
    console.log('entra por run_data_migration');
    var datetime = new Date().toISOString().replace(/\T.+/,'');
    const fit = new Fit();
    fit.migrateData("2018-12-06", datetime).then(function(result){
        fit.saveData(result);
        }).catch(function(err){
        console.log("catch error migrate data"+err);
    });

/*    const git = new Git();
    git.migrateData("2018-12-06", datetime).then(function(result){
        git.saveData(result);
        }).catch(function(err){
        console.log("catch error migrate data"+err);
    });
*/
    console.log("termina el proceso");
};

run_data_migration();