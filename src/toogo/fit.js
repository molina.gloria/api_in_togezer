import { connection, Schema, connect } from 'mongoose';
const { request, GraphQLClient } = require('graphql-request')
const fitQuery = `query getTripsToTime($startDate: String!, $endDate: String!, $limit: Int!, $offset: Int!)
{
    tripsToTime(
    startDate: $startDate
    endDate: $endDate
    limit: $limit
    offset: $offset
    ) {
        totalCount
        trips {
            hash
            urn
            code
            name
            programNumber
            programName
            description
            seasonality
            category
            level
            type
            tag
            media {
                url
                filename
                description
            }
            dmc {
                name
                slug
                tel
                email
            }
            client {
                name
                slug
            }
            currency {
                name
                code
            }
            minDaysNotice
            filepath
            duration
            startPlace {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            endPlace {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            places {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            regions {
                country {
                    name
                    description
                    slug
                    latitude
                    longitude
                    code
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            countries {
                name
                description
                slug
                latitude
                longitude
                code
                urn
            }
            dates {
                fromDate
                toDate
            }
            mainProgram
            subPrograms
            prices {
                supSgl
                minXBed1
                minChild
                supInfant
                rates {
                    fromPax
                    toPax
                    price
                }
            }
            itinerary {
                day
                products {
                    name
                    category
                    level
                    tag
                    policies {
                        age {
                            min
                            max
                        }
                        pax {
                            min
                            max
                        }
                        stay {
                            min
                            max
                        }
                        bookingNotice {
                            min
                        }
                    }
                    filepath
                    supplier {
                        name
                        category
                        address {
                            items {
                                type
                                properties {
                                    streetAddress
                                    extendedAddress
                                    locality
                                    region
                                    postalCode
                                    longitude
                                    latitude
                                }
                            }
                        }
                        licenseNumber
                        websiteURL
                        logoURL
                        fax
                        media {
                            url
                            filename
                            description
                        }
                        description
                        information {
                            name
                            description
                        }
                    }
                    parent
                }
            }
            programDetail {
                day
                description
                included
                places {
                    region {
                        country {
                            name
                            description
                            slug
                            latitude
                            longitude
                            code
                            urn
                        }
                        name
                        description
                        slug
                        latitude
                        longitude
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                media {
                    url
                    filename
                    description
                }
                startTime
                endTime
                distance
                decline
                products {
                    ref
                }
            }
            information {
                name
                description
            }
            age {
                minAge
                maxAge
                maxAgeChild
                maxAgeInfant
            }
        }
    }
}`; 

export default class Fit {
    migrateData(fromDate, toDate){
        var dataObtained = this.obtainData(fromDate, toDate);
        return new Promise(function(resolve, reject){
            resolve(dataObtained);
        });

        return promise;    
    }
    obtainData(fromDate, toDate){
        const client = new GraphQLClient("https://dev-api.toogo.in/fit", {headers: { "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImNsaWVudElkIjpbIjEzZGEzYzZlMGY4MDRhMTVlNjgwY2QwMzYxZjIzZTU2LTIxMzU0IiwiMTNkYTNjNmUwZjgwNGExNWU2ODBjZDAzNjFmMjNlNTYtOTk1IiwiMWFlMmM4MGIxNDE0YmFlMTU1MzcyNTU1OWIwNzZlYjItMTExODMiLCIxYWUyYzgwYjE0MTRiYWUxNTUzNzI1NTU5YjA3NmViMi0zMzQiLCI0MDhlZWFiMDJiODc4MTFkMDJiODkyODgwZmNhYTRlYS0zOTkiLCI5OWFiZjI4Y2YxY2M1MzZmNTA2OWQ5NTAxYzY1ZDI1YS05ODQiLCJkMTMyNWQwMGU4ODU1MGZlMjM2YWY5MTE3ZGEwNTJmMy0zMjEzIiwiZGRkZGVmOGY2NzJmMzhkYjk3MjdmNjJmMGZlODY0YmUtMTI1MyIsImYxNDBhZjg5MDJlMjZiYzE4ZWY0YzU5OGEzNjU4YjNhLTYwNiIsImY3ZGUzMDVkOThkNDE3YTZjZTNjYzQ3MzY0YWI3N2JjLTE3MTIiXSwiZG1jTGlzdCI6WyIxM2RhM2M2ZTBmODA0YTE1ZTY4MGNkMDM2MWYyM2U1NiIsIjFhZTJjODBiMTQxNGJhZTE1NTM3MjU1NTliMDc2ZWIyIiwiNDA4ZWVhYjAyYjg3ODExZDAyYjg5Mjg4MGZjYWE0ZWEiLCI5OWFiZjI4Y2YxY2M1MzZmNTA2OWQ5NTAxYzY1ZDI1YSIsImQxMzI1ZDAwZTg4NTUwZmUyMzZhZjkxMTdkYTA1MmYzIiwiZGRkZGVmOGY2NzJmMzhkYjk3MjdmNjJmMGZlODY0YmUiLCJmMTQwYWY4OTAyZTI2YmMxOGVmNGM1OThhMzY1OGIzYSIsImY3ZGUzMDVkOThkNDE3YTZjZTNjYzQ3MzY0YWI3N2JjIl0sImxhbmdMaXN0IjpbIkZSIl0sInJvbGUiOiJjbGllbnQiLCJ0cmlwS2luZCI6IkZJVCJ9LCJpYXQiOjE1NDU5NDk1NTYsImV4cCI6MTU1MTEzMzU1Nn0.5e39YtFCepRAPpYf5bDaFGX4MozLuyS0ykZCxvdAZMc"}});
        const variables = {
            startDate: fromDate, 
            endDate: toDate, 
            limit: 5,
            offset: 0,
        };
        return new Promise (function(resolve, reject){
            console.log("entra por promise");
            var data = client.request(fitQuery, variables);
            console.log("entra por data");
            resolve(data);
        }); 
    }

    saveData(jsonData) { 
        var url = "mongodb://admin:Trudy8_CPIs@52.51.170.38:27017/tgz_db_v2_DEV?authSource=admin";
        connect(url, { useNewUrlParser: true });
        var db = connection;
        db.on('error', console.error.bind(console, 'connection error: '));
        db.once('open', function(){
            jsonData.tripsToTime.trips.forEach(element => {
                element.productType = "fit";
                element.toogoTripID = element.hash;
                delete element.hash;
                element.tribes = element.dmc;
                delete element.dmc;
                element.prices.currency = element.currency;
                delete element.currency;
                element.program = [];
                element.programDetail.forEach(programElement => {
                    var dateProgram = [];
                    dateProgram.push({
                        description: programElement.description,
                        included: programElement.included
                    });
                    element.program.push({
                        date: programElement.day,
                        startTime: programElement.startTime,
                        endTime: programElement.endTime,
                        places: programElement.places,
                        media: programElement.media,
                        dateProgram: dateProgram,
                        distance: programElement.distance,
                        decline: programElement.decline,
                        detailProducts: programElement.products
                    });
                });
                delete element.programDetail;

                element.media.forEach(mediaElement => {
                    mediaElement.value = mediaElement.url;
                    mediaElement.picture = mediaElement.filename;
                    delete mediaElement.url;
                    delete mediaElement.filename;
                });
                element.translations = [];
                element.translations.push({
                    season: element.seasonality,
                    description: element.description
                });
                delete element.seasonality;
                delete element.description;

                element.dynamicInformation = [];
                element.information.forEach(informationElement => {
                    element.dynamicInformation.push({
                        title: informationElement.name,
                        value: informationElement.description
                    });    
                });
                delete element.information;
            });     
            console.log(jsonData.tripsToTime.trips);   
            db.collection("products").insertMany(jsonData.tripsToTime.trips);
        });  
    }
}