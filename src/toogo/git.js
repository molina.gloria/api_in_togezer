import { connection, Schema, connect } from 'mongoose';
const { request, GraphQLClient } = require('graphql-request')
const gitQuery = `query getTripsToTime($startDate: String!, $endDate: String!, $limit: Int!, $offset: Int!)
{
    tripsToTime(
    startDate: $startDate
    endDate: $endDate
    limit: $limit
    offset: $offset
    ) {
        totalCount
        trips {
            hash
            urn
            code
            name
            programNumber
            programName
            description
            seasonality
            category
            level
            type
            tag
            media {
                url
                filename
                description
            }
            dmc {
                name
                slug
                tel
                email
            }
            client {
                name
                slug
            }
            currency {
                name
                code
            }
            minDaysNotice
            filepath
            duration
            startPlace {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            endPlace {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            places {
                region {
                    country {
                        name
                        description
                        slug
                        latitude
                        longitude
                        code
                        urn
                    }
                    name
                    description
                    slug
                    latitude
                    longitude
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            regions {
                country {
                    name
                    description
                    slug
                    latitude
                    longitude
                    code
                    urn
                }
                name
                description
                slug
                latitude
                longitude
                urn
            }
            countries {
                name
                description
                slug
                latitude
                longitude
                code
                urn
            }
            dates {
                fromDate
                toDate
            }
            mainProgram
            subPrograms
            prices {
                supSgl
                minXBed1
                minChild
                supInfant
                rates {
                    fromPax
                    toPax
                    price
                }
            }
            itinerary {
                day
                products {
                    name
                    category
                    level
                    tag
                    policies {
                        age {
                            min
                            max
                        }
                        pax {
                            min
                            max
                        }
                        stay {
                            min
                            max
                        }
                        bookingNotice {
                            min
                        }
                    }
                    filepath
                    supplier {
                        name
                        category
                        address {
                            items {
                                type
                                properties {
                                    streetAddress
                                    extendedAddress
                                    locality
                                    region
                                    postalCode
                                    longitude
                                    latitude
                                }
                            }
                        }
                        licenseNumber
                        websiteURL
                        logoURL
                        fax
                        media {
                            url
                            filename
                            description
                        }
                        description
                        information {
                            name
                            description
                        }
                    }
                    parent
                }
            }
            programDetail {
                date
                description
                included
                    title
                    distance
                    duration            
            }
            information {
                name
                description
            }
            age {
                minAge
                maxAge
                maxAgeChild
                maxAgeInfant
            }
            pax{
                min
                max
                current
                maybe
            }
        }
    }
}`; 

export default class Git {
    migrateData(fromDate, toDate){
        var dataObtained = this.obtainData(fromDate, toDate);
        return new Promise(function(resolve, reject){
            resolve(dataObtained);
        });

        return promise;    
    }
    obtainData(fromDate, toDate){
        const client = new GraphQLClient("https://dev-api.toogo.in/git", {headers: {"Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImRtY0xpc3QiOlsiMTNkYTNjNmUwZjgwNGExNWU2ODBjZDAzNjFmMjNlNTYiXSwibGFuZ0xpc3QiOlsiRVMiXSwicm9sZSI6ImNsaWVudCIsInRyaXBLaW5kIjoiR0lUIn0sImlhdCI6MTU0NTk0OTYxNCwiZXhwIjoxNTUxMTMzNjE0fQ.FJhNlrPImv1MHVk-zXNG_ZgJ5YLChVVO0RoBmIzI-pM"}});
        const variables = {
            startDate: fromDate, 
            endDate: toDate, 
            limit: 20,
            offset: 0,
        };
        var data = client.request(gitQuery, variables);
        return new Promise (function(resolve, reject){
            resolve(data);
        }); 
    }

    saveData(jsonData) { 
        var url = "mongodb://admin:Trudy8_CPIs@52.51.170.38:27017/tgz_db_v2_DEV?authSource=admin";
        connect(url, { useNewUrlParser: true });
        var db = connection;
        db.on('error', console.error.bind(console, 'connection error: '));
        db.once('open', function(){
            jsonData.tripsToTime.trips.forEach(element => {
                element.productType = "git";
                element.toogoTripID = element.hash;
                delete element.hash;
                element.tribes = element.dmc;
                delete element.dmc;
                element.prices.currency = element.currency;
                delete element.currency;
                element.program = [];
                element.programDetail.forEach(programElement => {
                    var dateProgram = [];
                    dateProgram.push({
                        description: programElement.description,
                        included: programElement.included,
                        title: programElement.title,
                        duration: programElement.duration
                    });
                    element.program.push({
                        date: programElement.date,
                        dateProgram: dateProgram,
                        distance: programElement.distance,
                        detailProducts: programElement.products
                    });
                });
                delete element.programDetail;

                element.media.forEach(mediaElement => {
                    mediaElement.value = mediaElement.url;
                    mediaElement.picture = mediaElement.filename;
                    delete mediaElement.url;
                    delete mediaElement.filename;
                });
                element.translations = [];
                element.translations.push({
                    season: element.seasonality,
                    description: element.description
                });
                delete element.seasonality;
                delete element.description;

                element.dynamicInformation = [];
                element.information.forEach(informationElement => {
                    element.dynamicInformation.push({
                        title: informationElement.name,
                        value: informationElement.description
                    });    
                });
                delete element.information;
            });     
            console.log(jsonData.tripsToTime.trips);   
            db.collection("products").insertMany(jsonData.tripsToTime.trips);
        });  
    }
}